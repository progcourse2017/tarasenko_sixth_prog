#include "stdafx.h"
#include <iostream>
#include <ctime>
using namespace std;
int Fib(int count)
{
	if (count == 0) return 0;
	if (count == 1) return 1;
	return Fib(count - 1) + Fib(count - 2);
}
int main()
{
	setlocale(LC_ALL, "russian");
	int count;
	cout << "введите число:" << endl;
	cin >> count;
	int start_time = clock();
	for (int i = 0; i <= count; i++)
	{
		Fib(i);
		cout << Fib(i) << "  ";
	}
	int end_time = clock();
	system("pause");
	cout << endl;
	int search_time = end_time - start_time;
	cout << search_time << endl;
	system("pause");
	return 0;
}
