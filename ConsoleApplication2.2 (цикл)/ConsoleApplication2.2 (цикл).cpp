#include "stdafx.h"
#include <iostream>
#include <ctime>
using namespace std;


int main()
{
	setlocale(LC_ALL, "russian");
	int count;
	cout << "введите число:" << endl;
	cin >> count;
	int *Fib= new int [count];
	int start_time = clock();
	for (int i = 0; i <= count; i++)
	{
		if (i == 0) 
			Fib[i]=0;
		else if (i == 1) 
			Fib[i]=1;
		else
		Fib[i] = Fib[i - 1] + Fib[i - 2];

		cout << Fib[i] << "  ";
	}
	int end_time = clock();
	system("pause");
	cout << endl;
	int search_time = end_time - start_time;
	cout << search_time << endl;
	system("pause");
    return 0;
}

